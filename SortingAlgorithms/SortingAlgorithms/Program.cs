﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SortingAlgorithms
{
    class Program
    {
        static void Main(string[] args)
        {
            //-----initialise ArraySorting class-----
            var ArraySorting = new Algorithms();
            ArraySorting.ArraySorted += DisplayArray.DisplayArr;
            ArraySorting.ArraySorted += DisplayArray.SaveText;
            ArraySorting.ArraySorted += DisplayArray.SaveCSV;

            ClearFiles();

            //-----initialise testing arrays-----
            int[][] testingArrays = new int[][]
            {
                new int[1],
                new int[10],
                new int[20],
                new int[40],
                new int[60],
                new int[80],
                new int[100],
                new int[200],
                new int[400],
                new int[600],
                new int[800],
                new int[1000],
                new int[2000],
                new int[4000],
                new int[6000],
                new int[8000],
                new int[10000],
                new int[15000],
                new int[20000],
                new int[25000],
                new int[30000],
                new int[35000],
                new int[40000],
                new int[45000],
                new int[50000]
            };
            Random random = new Random();
            for (int i = 0; i < testingArrays.Length; i++)
            {
                for (int k = 0; k < testingArrays[i].Length; k++)
                {
                    testingArrays[i][k] = random.Next(1, (int)Math.Min((decimal)int.MaxValue, (decimal)testingArrays[i].Length*10));
                }
            }

            //---Test sorting algorithm's performance
            for (int i = 0; i < testingArrays.Length; i++)
            {
                for (int k = 0; k < 5; k++)
                {
                    int[] testingArray = (int[])testingArrays[i].Clone();
                    Console.WriteLine("Selection size:{0}", testingArray.Length);
                    ArraySorting.SelectionSort(testingArray);

                    Console.WriteLine();
                }

            }
            for (int i = 0; testingArrays[i].Length < 25000; i++)
            {
                for (int k = 0; k < 5; k++)
                {
                    int[] testingArray = (int[])testingArrays[i].Clone();
                    Console.WriteLine("Recursive Selection size:{0}", testingArray.Length);
                    ArraySorting.RecSelectionSort(testingArray);

                    Console.WriteLine();
                }

            }
            for (int i = 0; i < testingArrays.Length; i++)
            {
                for (int k = 0; k < 5; k++)
                {
                    int[] testingArray = (int[])testingArrays[i].Clone();
                    Console.WriteLine("DoubleSelection size:{0}", testingArray.Length);
                    ArraySorting.DoubleSelectionSort(testingArray);

                    Console.WriteLine();


                }

            }
            for (int i = 0; i < testingArrays.Length; i++)
            {
                for (int k = 0; k < 5; k++)
                {
                    int[] testingArray = (int[])testingArrays[i].Clone();
                    testingArray = (int[])testingArrays[i].Clone();
                    Console.WriteLine("Merge size:{0}", testingArray.Length);
                    ArraySorting.MergeSort(testingArray);

                    Console.WriteLine();
                }

            }

            

            
        }

        static void ClearFiles()
        {
            using (var File = new StreamWriter(DisplayArray.TextFile))
            {
                File.WriteLine("Sorted array results");
                File.WriteLine();
            }
            using (StreamWriter File = new StreamWriter(DisplayArray.CSVFile))
            {
                File.WriteLine("Sorted Array Results");
                File.WriteLine();
            }
        }

    }
}
