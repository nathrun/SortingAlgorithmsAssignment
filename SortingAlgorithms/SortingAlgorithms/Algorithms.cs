﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SortingAlgorithms
{
    public class Algorithms
    {
        public event EventHandler<Result> ArraySorted;

        //  ---Normal Selection Sort---
        public void SelectionSort(int[] input)
        {
            var Watch = Stopwatch.StartNew();
            for (int i = 0; i < input.Length - 1; i++)
            {
                var max = i;
                for (int k = i + 1; k < input.Length; k++)
                {
                    if (input[k] > input[max])
                    {
                        max = k;
                    }
                }
                Swop(ref input, max, i);
            }
            Watch.Stop();
            Console.WriteLine("IsHighResolution: {0}", Stopwatch.IsHighResolution);
            OnArraySorted(new Result() { arr = input, Title = "Normal Selection Sort;;;; ArraySize:; " + input.Length, elapsedTime=Watch.ElapsedTicks });
        }


        //  ---Recursive Selection Sort---
        public void RecSelectionSort(int[] input)
        {
            var stopwatch = Stopwatch.StartNew();
            RecSelection(input, 0);
            stopwatch.Stop();
            OnArraySorted(new Result() { arr = input, Title = "Recurive Selection Sort;;;; ArraySize:; " + input.Length, elapsedTime = stopwatch.ElapsedTicks });
        }

        private void RecSelection(int[] input, int startIndex)
        {
            if (input.Length <= 1 || startIndex == input.Length-1)
                return;

            var maxInList = MaxInListIndex(input, startIndex);
            Swop(ref input, startIndex, maxInList);
            RecSelection(input, ++startIndex);
           
        }

        private int MaxInListIndex(int[] input, int startIndex)
        {
            if (startIndex == input.Length)
                return -1;

            var MaxOfRest = MaxInListIndex(input, startIndex + 1);
            if (MaxOfRest == -1)
                return startIndex;
            if (input[MaxOfRest] > input[startIndex])
                return MaxOfRest;
            else
                return startIndex;
        }


        //  ---Double Selection Sort---
        public void DoubleSelectionSort(int[] input)
        {
            var Watch = Stopwatch.StartNew();
            var end = input.Length-1;
            for (int start = 0; start <= end; start++)
            {
                int min = start, max = start; //store position of min and max
                for (int i = start; i <= end; i++)
                {
                    if (min != i)
                        if (input[min] > input[i])
                            min = i;

                    if (max != i)
                        if (input[max] < input[i])
                            max = i;

                }
                if (max != start)
                    if (min == start)
                        min = max;
                    Swop(ref input, max, start);
                if (min != end)
                    Swop(ref input, min, end);
                end--;
            }
            Watch.Stop();
            Console.WriteLine("IsHighResolution: {0}", Stopwatch.IsHighResolution);
            OnArraySorted(new Result() { arr = input, Title = "DoubleSelection Sort;;;; ArraySize:; " + input.Length, elapsedTime = Watch.ElapsedTicks });
        }


        //  ---Merge Sort---
        public void MergeSort(int[] input)
        {
            var Watch = Stopwatch.StartNew();
            MergeSortRec(ref input, 0, input.Length-1);
            Watch.Stop();
            Console.WriteLine("IsHighResolution: {0}", Stopwatch.IsHighResolution);
            OnArraySorted(new Result() { arr = input, Title = "Merge Sort;;;; ArraySize:; "+input.Length, elapsedTime = Watch.ElapsedTicks });
        }

        private void MergeSortRec(ref int[] arr, int start, int end)
        {
            if (start >= end)
                return;
            int mid = (start + end) / 2;
            MergeSortRec(ref arr, start, mid);
            MergeSortRec(ref arr, mid + 1, end);

            Merge(ref arr, start, mid, end);
        }

        private void Merge(ref int[] arr, int start, int mid, int end)
        {
            if (start == end)
                return;

            int x = start, m = mid + 1;
            int[] Scratch = new int[end - start + 1];
            int k = 0;

            for (int i = start; i <= end; i++)
            {
                if (x > mid)
                    Scratch[k] = arr[m++];
                else if (m > end)
                    Scratch[k] = arr[x++];
                else if (arr[x] > arr[m])
                    Scratch[k] = arr[x++];
                else
                    Scratch[k] = arr[m++];
                k++;
            }

            for (x = 0; x < k; x++)
            {
                arr[start++] = Scratch[x];
            }
        }


        //  ---Helper methods---
        private void Swop(ref int[] list, int index1, int index2)
        {
            var temp = list[index1];
            list[index1] = list[index2];
            list[index2] = temp;
        }

        protected virtual void OnArraySorted(Result result)
        {
            ArraySorted?.Invoke(this, result);
        }
    }
}
