﻿using System;
using System.IO;

namespace SortingAlgorithms
{
    public static class DisplayArray
    {
        public static string TextFile = "Results.txt";
        public static string CSVFile = "Results.csv";

        public static void DisplayArr(object source, Result result)
        {
            int n = result.arr.Length;
            if (result.arr.Length <= 100)
                Console.WriteLine(String.Join(",",result.arr));
            else
                Console.WriteLine("array size is larger than 100");

            Console.WriteLine("Ticks: {0}",result.elapsedTime);
            Console.WriteLine();
        }

        public static void SaveText(object source, Result result)
        {
            using (StreamWriter writer = File.AppendText(TextFile))
            {
                var n = result.arr.Length;
                writer.WriteLine(result.Title);
                writer.WriteLine("Ticks: " + result.elapsedTime);
                if (result.arr.Length <= 1000)

                    writer.WriteLine(String.Join(",", result.arr));
                else
                    writer.WriteLine("Array size above 1000");
                writer.WriteLine();
                writer.WriteLine("---------------------");
            }
        }

        public static void SaveCSV(object source, Result result)
        {
            using (StreamWriter writer = File.AppendText(CSVFile))
            {
                var n = result.arr.Length;
                writer.WriteLine(result.Title+";;Ticks:;"+result.elapsedTime);
                writer.WriteLine();
                writer.WriteLine("----------------------------------------------");
            }
        }
    }
}
